provider "google" {
  
credentials = "${file("account.json")}"
  
project = "tonal-baton-222317"
  
region = "europe-west1"
  
zone = "europe-west1-b"
}

resource "google_compute_instance" "vm_instance" {
  name         = "slave-${count.index+1}"
  machine_type = "n1-standard-2"
  count=3

  boot_disk {
    initialize_params {
      image = "ubuntu-os-cloud/ubuntu-1604-lts"
      size = "30"
    }
  }
  network_interface {
  network = "${google_compute_network.default.name}"
  access_config = {}
}

}

resource "google_compute_firewall" "default" {
  name    = "yac"
  network = "${google_compute_network.default.name}"

  allow {
    protocol = "icmp"
  }

  allow {
    protocol = "tcp"
    ports    = ["80", "8080", "1000-2000"]
  }
  allow {
    protocol = "udp"
    ports    = ["80", "8080", "1000-2000"]
  }

}

resource "google_compute_network" "default" {
  name                    = "yac"
  auto_create_subnetworks = "true"
 
}
output "ip" {
   value = "${concat("${google_compute_instance.vm_instance.*.network_interface.0.network_ip}", "${google_compute_instance.vm_instance.*.network_interface.0.access_config.0.nat_ip}" ) }"
}